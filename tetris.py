#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Author: Forat Al-Hellali <foratseif@gmail.com>
# Desc: Tetris game inside the terminal
# Started: 30.april.17 15:47
# Done:
# Reason for creating: Still no Life :/ ..

# includes ==============================================
import curses, time, random
import sys

# global variables ======================================
WHOLE="██"
HOLLOW="▒▒"
height=0
width=0
stdscr=None
CHEAT_MODE = False

global debug_mode, color_mode, scale, height_offset
scale = 1
height_offset = 0
debug_mode = False
color_mode = False
shadow_mode = False

# keyboard values
UP=1; DOWN=2; LEFT=3; RIGHT=4; SPACE=5;

# colors
RED=1; LBLUE=2; DBLUE=3; GREEN=4;
ORANGE=5;YELLOW=6; PURPLE=7; SHADOW=8

# define pieces =========================================
PIECES = [
    (LBLUE  , WHOLE, [[0, 1], [0, 0], [0, 2], [0, 3]]), # long piece
    (DBLUE  , WHOLE, [[1, 1], [1, 0], [0, 0], [1, 2]]), # reverse L
    (ORANGE , WHOLE, [[1, 1], [1, 2], [1, 0], [0, 2]]), # L
    (YELLOW , WHOLE, [[0, 0], [1, 0], [0, 1], [1, 1]]), # square
    (GREEN  , WHOLE, [[1, 1], [1, 0], [0, 1], [0, 2]]), # swigly
    (PURPLE , WHOLE, [[1, 1], [1, 0], [0, 1], [1, 2]]), # T
    (RED    , WHOLE, [[1, 1], [0, 1], [0, 0], [1, 2]])  # reverse swigly
]

# methods for curses ====================================

# init the game window
def init(autoscale):
    global stdscr, height, width, scale, height_offset
    stdscr = curses.initscr()
    curses.noecho()
    curses.curs_set(0)
    stdscr.keypad(True)

    height, width = stdscr.getmaxyx()
    width /= 2

    if autoscale:
        while width/scale > 10 and height/scale > 20:
            scale += 1
        width /= scale
        height_offset = height%scale
        height /= scale

    height = int(height)
    width  = int(width)
    height_offset  = int(height_offset)

    if color_mode:
        curses.start_color()
        curses.init_pair(RED   , curses.COLOR_RED,     curses.COLOR_BLACK);
        curses.init_pair(LBLUE , curses.COLOR_CYAN,    curses.COLOR_BLACK);
        curses.init_pair(DBLUE , curses.COLOR_BLUE,    curses.COLOR_BLACK);
        curses.init_pair(GREEN , curses.COLOR_GREEN,   curses.COLOR_BLACK);
        curses.init_pair(ORANGE, curses.COLOR_WHITE,   curses.COLOR_BLACK);
        curses.init_pair(YELLOW, curses.COLOR_YELLOW,  curses.COLOR_BLACK);
        curses.init_pair(PURPLE, curses.COLOR_MAGENTA, curses.COLOR_BLACK);
        curses.init_pair(SHADOW, curses.COLOR_WHITE,   curses.COLOR_BLACK);


# end the game window
def end():
    if stdscr:
        curses.endwin()

# addstr that avoids error when printing in bottom right corner
def _addstr(y, x, val, color=0):
    try:
        for i in range(scale):
            if color == 0:
                stdscr.addstr(int(height_offset+(y*scale)+i), int((x*2)*scale), val*scale)
            else:
                stdscr.addstr(int(height_offset+(y*scale)+i), int((x*2)*scale), val*scale, curses.color_pair(color))
    except curses.error:
        stdscr.move(0,0)

# render
def render(render, clear=[]):

    # clear boxes from last run
    for y, x, color, fill in clear:
        if color_mode:
            _addstr(y, x, "  ", color)
        else:
            _addstr(y, x, "  ")

    # print new boxes
    for y, x, color, fill in render:
        if color_mode:
            _addstr(y, x, fill, color)
        else:
            _addstr(y, x, fill)

    # output snake
    stdscr.refresh()

# set getch timeout
def set_timeout(to):
    stdscr.timeout(to)

# listen for keypress
def keyboard():
    global CHEAT_MODE

    c = stdscr.getch()
    if debug_mode:
        stdscr.addstr(1, 0, "keycode: "+str(c)+"   ")
        stdscr.addstr(2, 0, "CHEAT_MODE: "+str(CHEAT_MODE)+" ")

    if c == curses.KEY_UP or c == 107:
        return UP
    elif c == curses.KEY_DOWN or c == 106:
        return DOWN
    elif c == curses.KEY_LEFT or c == 104:
        return LEFT
    elif c == curses.KEY_RIGHT or c == 108:
        return RIGHT
    elif c == curses.KEY_BACKSPACE or c == 32 or c == 10:
        return SPACE
    elif c == 9:
        CHEAT_MODE = not CHEAT_MODE
    return 0


# methods for pieces ====================================

# make a random piece
def make_piece():
    ret = []

    i = random.randint(0, len(PIECES)-1)

    if CHEAT_MODE:
        i = 0

    # clone piece
    piece_template = PIECES[i];
    for i in piece_template[2]:
        ret.append([i[0], i[1], piece_template[0], piece_template[1]])

    return ret

# make a shadow piece
def make_shadow(piece, lines):
    ret = []

    # clone piece
    for i in piece:
        ret.append([i[0], i[1], i[2], HOLLOW])

    # move shadow piece to bottom
    while not crashing(ret, lines):
        move_piece(ret, 1, 0)

    return ret

# move piece +x +y
def move_piece(piece, y, x):
    for i in range(len(piece)):
        piece[i][0] = piece[i][0] + y
        piece[i][1] = piece[i][1] + x

    return piece

# rotate piece (right: +1 | left: -1)
def rot_piece(piece, d):
    for i in range(len(piece)):
        offset_y = piece[0][0]
        offset_x = piece[0][1]

        # move to origin
        piece[i][0] = piece[i][0] - offset_y
        piece[i][1] = piece[i][1] - offset_x

        # rotate around axis
        tmp = piece[i][1]
        piece[i][1] = d*piece[i][0]
        piece[i][0] = -1*d*tmp

        # add previous offset
        piece[i][0] = piece[i][0] + offset_y
        piece[i][1] = piece[i][1] + offset_x

    return piece

# piece touching ground
def touching_ground(piece):
    for cords in piece:
        if(cords[0] == height-1):
            return True
    return False

# check if piece can move +dy +dx
def can_move(piece, lines, dy, dx):
    for cords in piece:
        y = cords[0]+dy
        x = cords[1]+dx
        if y in lines:
            for cord in lines[y]:
                if cord[0] == x:
                    return False

        if x > width-1:
            return False

        if x < 0:
            return False

    return True

# checks if piece can rotate
def can_rotate(piece, lines, d):
    new_piece = []
    for cords in piece:
        new_piece.append(cords[:])

    rot_piece(new_piece, d)
    return can_move(new_piece, lines, 0, 0) and not touching_ground(new_piece)

# check if is touching ground or blocks below
def crashing(piece, lines):
    return (not can_move(piece, lines, 1, 0)) or touching_ground(piece)

# check if piece is touching the ceiling
def touching_ceiling(piece):
    for cords in piece:
        if cords[0] == 0:
            return True
    return False

# methods for game ======================================

# add piece to lines
def add_to_lines(piece, lines):
    for bit in piece:
        y = bit[0]
        if not  y in lines:
            lines[y] = []
        lines[y].append((bit[1], bit[2], bit[3]))

# detect a whole line
def detect_whole_line(lines):
    fulls = []
    for y in lines:
        if len(lines[y]) == width:
            fulls.append(y)

    if len(fulls) > 0:
        blink_lines(lines, fulls)
        clear_lines(lines)
        for y in fulls:
            remove_line(y, lines)
        render_lines(lines)

    return len(fulls)

# blin animation for a line
def blink_lines(lines, ys):
    for i in range(3):
        clear_lines(lines)
        render_lines(lines, ys)
        time.sleep(0.05)
        render_lines(lines)
        time.sleep(0.05)


# render lines
def render_lines(lines, excludes=[]):
    a = []
    for y in lines:
        if y not in excludes:
            for x, c, f in lines[y]:
                a.append([y, x, c, f])
    render(a)

# clear lines
def clear_lines(lines):
    a = []
    for y in lines:
        for x, c, f in lines[y]:
            a.append([y, x, c, f])
    render([], a)

# remove line
def remove_line(y, lines):
    for i in range(int(y)-1, 0, -1):
        if i+1 in lines:
            if i in lines:
                lines[i+1] = lines[i]
            else:
                lines[i+1] = []

# main run function for the game
def run():

    lines = {}

    score = 0

    timeout = 500

    set_timeout(timeout)

    shadow_piece = None

    while True:
        piece = make_piece()
        move_piece(piece, 0, width/2 - 2)

        if shadow_mode:
            if shadow_piece is not None:
                render([], shadow_piece)
            shadow_piece = make_shadow(piece, lines)
            render(shadow_piece)

        render(piece)

        skip = False

        for i in range(height):
            # debug stuff
            if debug_mode:
                stdscr.addstr(0, 0, "                                                                            ")
                stdscr.addstr(0, 0, ("  "+str(i))[-3:]+" |")
                stdscr.addstr(0, 6, str([p[:2] for p in piece]))

            # stuff stuff
            t = time.time()
            while time.time()-t < timeout/1000.00:
                key = DOWN
                if skip is False:
                    key = keyboard()

                x = 0
                if key == RIGHT:
                    x = 1
                elif key == LEFT:
                    x = -1
                elif key == DOWN:
                    break
                elif key == UP:
                    if can_rotate(piece, lines, -1):
                        render([], piece)
                        rot_piece(piece, -1)
                        render(piece)
                elif key == SPACE:
                    skip = True

                if not can_move(piece, lines, 0, x):
                    x = 0

                render([], piece)
                move_piece(piece, 0, x)

                if shadow_mode:
                    if shadow_piece is not None:
                        render([], shadow_piece)
                    shadow_piece = make_shadow(piece, lines)
                    render(shadow_piece)
                render(piece)

            if crashing(piece, lines):
                if shadow_piece is not None:
                    shadow_piece = None

                if touching_ceiling(piece):
                    return score

                add_to_lines(piece, lines)

                x = detect_whole_line(lines)
                if x > 0:
                    score = score + x*width
                    timeout -= x*10
                    if timeout < 100:
                        timeout = 100
                    set_timeout(timeout)

                break
            else:
                if shadow_mode:
                    if shadow_piece is not None:
                        render([], shadow_piece)
                    shadow_piece = make_shadow(piece, lines)
                    render(shadow_piece)

                render([], piece)
                move_piece(piece, 1, 0)
                render(piece)



# start the main program ================================
if __name__ == '__main__':
    # check python version
    if sys.version_info[0] < 3:
        import locale
        locale.setlocale(locale.LC_ALL,"") #compensate for utf8 chars

    #parse arguments
    debug_mode     = ('--debug'     in sys.argv or '-d'  in sys.argv)
    color_mode     = ('--color'     in sys.argv or '-nc' in sys.argv)
    shadow_mode    = ('--shadow'    in sys.argv or '-s'  in sys.argv)
    autoscale_mode = ('--autoscale' in sys.argv or '-as' in sys.argv)

    #import traceback if in debug_mode
    if debug_mode:
        import traceback

    # init the game
    init(autoscale_mode)

    # run the game
    try:
        score = run()
        message = "You lose :&\nScore: " + str(score)

    except KeyboardInterrupt as e:
        message = 'You quit.'
    except:
        end()

        if debug_mode:
            traceback.print_exc()
        else:
            print("The game crashed :/")
        exit(1)

    # end curses
    end()

    print(message)
