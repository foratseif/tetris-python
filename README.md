# tetris-python
Tetris game in terminal, created with python and python-curses.

# How to run
Either of the following:
- ```python3 tetris.py```
- ```python tetris.py```
- ```./tetris.py```

# Flahs
- **debug_mode:**     ```--debug```     or ```-d``` shows some debugging info and prints trackeback for exceptions
- **color_mode:**     ```--color```     or ```-nc``` uses xterm colors to display colors
- **shadow_mode:**    ```--shadow```    or ```-s``` shows where the tetris brick is going to land
- **autoscale_mode:** ```--autoscale``` or ```-as``` scales the terminal to match original tetris window a much as possible
